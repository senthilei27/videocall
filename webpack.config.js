var webpack = require('webpack');
var path = require('path');

var config = {
    entry: './src/index.js',
    mode: 'development',
    devtool: 'eval-source-map',
    target: 'web',
    output: {
        path: path.resolve(__dirname, 'src'),
        filename: 'main.js'
    },
    plugins: [
        new webpack.ProvidePlugin({
            process: 'process/browser',
        }),
        new webpack.ProvidePlugin({
            Buffer: ['buffer', 'Buffer'],
        }),
        new webpack.DefinePlugin({
            'process.browser': JSON.stringify(true),
            'process.env': {
                NODE_ENV: JSON.stringify(process.env.NODE_ENV),
            },
        }),
    ],
    resolve: {
        extensions: ['.ts', '.js'],
        fallback: {
            "stream": require.resolve("stream-browserify"),
            'readable-stream': require.resolve('readable-stream'),
            'base64-js': require.resolve('base64-js'),
            "buffer": require.resolve("buffer")
        }
    },
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            }
        ]
    }
};

module.exports = config;