import React, { createContext, useEffect, useState, useMemo } from 'react';
import useSymbl from "./useSymbl/useSymbl";
import useVideoContext from "../../hooks/useVideoContext/useVideoContext";
// import MicrophoneStream from "microphone-stream";
// import { Buffer } from "buffer";
// import { TranscribeStreamingClient, StartStreamTranscriptionCommand } from '@aws-sdk/client-transcribe-streaming';
// import CryptoJS from 'crypto-js';
// import queryString from 'query-string';
// import { sampleWebSocket } from './useSymbl/sampleWebSocket';
// import { startRecording, stopRecording } from './useSymbl/sampleSDK';
import TranscribeController from './useSymbl/anotherSdk/transcriptSDK';

const symblConnectionMode = 'websocket_api';

const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
const recognition = new SpeechRecognition();

export const SymblContext = createContext(null);

export function SymblProvider({
    options, roomName, children, onError = () => {
    }
}) {
    const onErrorCallback = (error) => {
        console.log(`ERROR: ${error.message}`, error);
        onError(error);
    };

    const [closedCaptionResponse, setClosedCaptionResponse] = useState({});
    const [messages, setMessages] = useState([]);
    const [newMessages, setNewMessages] = useState([]);
    const [webSocket, setWebSocket] = React.useState();
    const [inputSampleRate, setInputSampleRate] = React.useState();
    const [transcriptText, setTranscript] = useState('');
    const transcribeController = useMemo(() => new TranscribeController(), []);

    const {
        room: { localParticipant }
    } = useVideoContext();
    // const [audioBuffer] = useState(
    //     (function () {
    //         let buffer = [];
    //         function add(raw) {
    //             buffer = buffer.concat(...raw);
    //             return buffer;
    //         }
    //         function newBuffer() {
    //             console.log("resetting buffer");
    //             buffer = [];
    //         }

    //         return {
    //             reset: function () {
    //                 newBuffer();
    //             },
    //             addData: function (raw) {
    //                 return add(raw);
    //             },
    //             getData: function () {
    //                 return buffer;
    //             }
    //         };
    //     })()
    // );

    const onResultCallback = (data) => {
        console.log('onResultCallback: ', data);
        if (data) {
            const { type } = data;
            if (type) {
                if (type === 'transcript_response') {
                    setClosedCaptionResponse(data)
                } else if (type === 'message_response') {
                    setMessages(data.messages)
                }
            }
        }
    }

    const onSpeechDetected = (data) => {
        console.log('onSpeechDetected: ', data);
        setClosedCaptionResponse(data)
    };

    const onMessageResponse = (newMessages) => {
        console.log('newMessages: ', newMessages);
        setNewMessages(newMessages);
        setMessages([messages, ...newMessages]);
    };

    const onConversationCompleted = (messages) => {
        console.log('Conversation completed.', messages);
    }

    const handlers = {
        onSpeechDetected,
        onMessageResponse,
        onConversationCompleted,
        onInsightResponse: (data) => {
            console.log(JSON.stringify(data))
        },
    };

    const {
        isConnected, connectionId, startedTime, startSymbl, stopSymbl, startSymblWebSocketApi, stopSymblWebSocketApi,
        muteSymbl, unMuteSymbl, isMute
    } =
        useSymbl(onErrorCallback, onResultCallback, { ...options });

    let transcribeClient = undefined;
    // let microphoneStream = new MicrophoneStream();
    ;
    const SAMPLE_RATE = 44100;

    // const startTranscript = () => {
    //     transcribeClient = new TranscribeStreamingClient({
    //         region: awsID.REGION,
    //     });
    // }
    // const stopTranscript = () => {

    // }
    // const createTranscribeClient = () => {
    //     transcribeClient = new TranscribeStreamingClient({
    //         region: 'us-west-2',
    //         accessKeyId: 'YOUR ACCESS KEY',
    //         secretAccessKey: 'YOUR SECRET KEY'
    //     });
    // }
    // let microphoneStream = null;
    // const createMicrophoneStream = async () => {
    //     microphoneStream = new MicrophoneStream();
    //     const rawMediaStream = await window.navigator.mediaDevices.getUserMedia({
    //         video: false,
    //         audio: true,
    //     })
    //     await microphoneStream.setStream(rawMediaStream);
    // }

    // const createMicrophoneStream = async (mediaRecorder) => {
    //     console.log("microphoneStream = ", microphoneStream);
    //     // Request microphone access
    //     const stream = await navigator.mediaDevices.getUserMedia({ audio: true });

    //     // Create a MediaRecorder instance
    //     mediaRecorder = new MediaRecorder(stream);

    //     // Handle captured audio data
    //     mediaRecorder.addEventListener('dataavailable', (event) => {
    //         const audioData = event.data;
    //         console.log("audioData = ", audioData);

    //     });

    //     mediaRecorder.start();
    //     // microphoneStream.setStream(
    //     //     await window.navigator.mediaDevices.getUserMedia({
    //     //         video: true,
    //     //         audio: true,
    //     //     })
    //     // );
    //     // window.navigator.mediaDevices.getUserMedia({
    //     //     video: false,
    //     //     audio: true,
    //     // }).then(async (stream) => {
    //     //     microphoneStream.setStream(stream);

    //     // })
    //     //     .catch(err => console.log("error = ", err))
    //     // await startTranscript();
    // }

    // const startTranscript = async (language, callback) => {
    //     transcribeClient = new TranscribeStreamingClient({
    //         region: "us-west-2",
    //     });
    //     const command = new StartStreamTranscriptionCommand({
    //         LanguageCode: "en-US",
    //         MediaEncoding: "pcm",
    //         MediaSampleRateHertz: 44100,
    //         AudioStream: { // AudioStream Union: only one key present
    //             AudioEvent: { // AudioEvent
    //                 AudioChunk: getAudioStream(),
    //             },
    //         }
    //     });
    //     const data = await transcribeClient.send(command);
    //     for await (const event of data.TranscriptResultStream) {
    //         for (const result of event.TranscriptEvent.Transcript.Results || []) {
    //             if (result.IsPartial === false) {
    //                 const noOfResults = result.Alternatives[0].Items.length;
    //                 for (let i = 0; i < noOfResults; i++) {
    //                     console.log(result.Alternatives[0].Items[i].Content);
    //                     callback(result.Alternatives[0].Items[i].Content + " ");
    //                 }
    //             }
    //         }
    //     }
    // }

    // const getAudioStream = async function* () {
    //     for await (const chunk of microphoneStream) {
    //         if (chunk.length <= SAMPLE_RATE) {
    //             yield {
    //                 AudioEvent: {
    //                     AudioChunk: encodePCMChunk(chunk),
    //                 },
    //             };
    //         }
    //     }
    // };

    // const encodePCMChunk = (chunk) => {
    //     const input = MicrophoneStream.toRaw(chunk);
    //     let offset = 0;
    //     const buffer = new ArrayBuffer(input.length * 2);
    //     const view = new DataView(buffer);
    //     for (let i = 0; i < input.length; i++, offset += 2) {
    //         let s = Math.max(-1, Math.min(1, input[i]));
    //         view.setInt16(offset, s < 0 ? s * 0x8000 : s * 0x7fff, true);
    //     }
    //     return Buffer.from(buffer);
    // };

    // const createPresignedURL = (method, host, path, service, payload, options) => {
    //     options = options || {};
    //     options.key = options.key || process.env.AWS_ACCESS_KEY_ID;
    //     options.secret = options.secret || process.env.AWS_SECRET_ACCESS_KEY;
    //     options.protocol = options.protocol || 'https';
    //     options.headers = options.headers || {};
    //     options.timestamp = options.timestamp || Date.now();
    //     options.region = options.region || process.env.AWS_REGION || 'us-west-2';
    //     options.expires = options.expires || 86400; // 24 hours
    //     options.headers = options.headers || {};

    //     // host is required
    //     options.headers.Host = host;

    //     var query = options.query ? queryString.parse(options.query) : {};
    //     query['X-Amz-Algorithm'] = 'AWS4-HMAC-SHA256';
    //     query['X-Amz-Credential'] = options.key + '/' + createCredentialScope(options.timestamp, options.region, service);
    //     query['X-Amz-Date'] = toTime(options.timestamp);
    //     query['X-Amz-Expires'] = options.expires;
    //     query['X-Amz-SignedHeaders'] = createSignedHeaders(options.headers);
    //     if (options.sessionToken) {
    //         query['X-Amz-Security-Token'] = options.sessionToken;
    //     }

    //     var canonicalRequest = createCanonicalRequest(method, path, query, options.headers, payload);
    //     var stringToSign = createStringToSign(options.timestamp, options.region, service, canonicalRequest);
    //     var signature = createSignature(options.secret, options.timestamp, options.region, service, stringToSign);
    //     query['X-Amz-Signature'] = signature;
    //     return options.protocol + '://' + host + path + '?' + queryString.stringify(query);
    // };


    // const createCredentialScope = (time, region, service) => {
    //     return [toDate(time), region, service, 'aws4_request'].join('/');
    // }

    // const createSignedHeaders = (headers) => {
    //     return Object.keys(headers).sort().map(function (name) {
    //         return name.toLowerCase().trim();
    //     }).join(';');
    // }

    // const createCanonicalRequest = (method, pathname, query, headers, payload) => {
    //     return [
    //         method.toUpperCase(),
    //         pathname,
    //         createCanonicalQueryString(query),
    //         createCanonicalHeaders(headers),
    //         createSignedHeaders(headers),
    //         payload
    //     ].join('\n');
    // }

    // const createCanonicalQueryString = (params) => {
    //     return Object.keys(params).sort().map(function (key) {
    //         return encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
    //     }).join('&');
    // };

    // const createCanonicalHeaders = (headers) => {
    //     return Object.keys(headers).sort().map(function (name) {
    //         return name.toLowerCase().trim() + ':' + headers[name].toString().trim() + '\n';
    //     }).join('');
    // }

    // const createStringToSign = (time, region, service, request) => {
    //     return [
    //         'AWS4-HMAC-SHA256',
    //         toTime(time),
    //         createCredentialScope(time, region, service),
    //         createHash(request, 'hex')
    //     ].join('\n');
    // }

    // const createSignature = (secret, time, region, service, stringToSign) => {
    //     var h1 = createHmacString('AWS4' + secret, toDate(time)); // date-key
    //     var h2 = createHmacString(h1, region); // region-key
    //     var h3 = createHmacString(h2, service); // service-key
    //     var h4 = createHmacString(h3, 'aws4_request'); // signing-key
    //     return createHmacString(h4, stringToSign, 'hex');
    // }

    // function toTime(time) {
    //     return new Date(time).toISOString().replace(/[:\-]|\.\d{3}/g, '');
    // }

    // function toDate(time) {
    //     return toTime(time).substring(0, 8);
    // }

    // function hmac(key, string, encoding) {
    //     return crypto.createHmac('sha256', key)
    //         .update(string, 'utf8')
    //         .digest(encoding);
    // }

    // const createHmacString = (privateKey, ts) => {
    //     const key = CryptoJS.enc.Utf8.parse(privateKey)
    //     const timestamp = CryptoJS.enc.Utf8.parse(ts)
    //     const hmac = CryptoJS.enc.Hex.stringify(CryptoJS.HmacSHA256(timestamp, key))

    //     //  const hmac = CryptoJS.HmacSHA256(ts, privateKey).toString(CryptoJS.enc.Hex)
    //     return hmac;
    // }

    // const createHash = (message) => {
    //     const hash = CryptoJS.enc.Hex.stringify(CryptoJS.SHA256(message));

    //     //  const hmac = CryptoJS.HmacSHA256(ts, privateKey).toString(CryptoJS.enc.Hex)
    //     return hash;
    // }


    useEffect(() => {
        let mediaRecorder;

        (async () => {
            // if (roomName) {
            //     if (symblConnectionMode === 'websocket_api') {
            //         await startSymblWebSocketApi(handlers, {
            //             meetingTitle: roomName,
            //             meetingId: btoa(roomName),
            //             handlers,
            //             localParticipant: {
            //                 name: localParticipant.identity
            //             },
            //             // signedURL: createPresignedURL(
            //             //     'GET',
            //             //     "transcribestreaming." + "us-west-2" + ".amazonaws.com:8443",
            //             //     '/stream-transcription-websocket',
            //             //     'transcribe',
            //             //     createHash('sha256'), {
            //             //     'key': "",
            //             //     'secret': "",
            //             //     'protocol': 'wss',
            //             //     'expires': 15,
            //             //     'region': "us-west-2",
            //             //     'query': "language-code=" + "en-US" + "&media-encoding=pcm&sample-rate=" + "44100"
            //             // }
            //             // )
            //         });
            //     } else {
            //         await startSymbl(roomName);
            //     }
            //     // createMicrophoneStream(mediaRecorder);
            //     // createTranscribeClient();
            //     // createMicrophoneStream();
            //     // await startStreaming(language, callback);
            // }
        })();
        // var h1 = createHmacString('AWS4' + 'AKIA37ZFJDY4JB56VAE', toDate(Date.now())); // date-key
        // console.log("h1= ", h1);
        // var signedURL = createPresignedURL(
        //     'GET',
        //     "transcribestreaming." + "us-west-2" + ".amazonaws.com:8443",
        //     '/stream-transcription-websocket',
        //     'transcribe',
        //     createHash('sha256'), {
        //     'key': "",
        //     'secret': "",
        //     'protocol': 'wss',
        //     'expires': 15,
        //     'region': "us-west-2",
        //     'query': "language-code=" + "en-US" + "&media-encoding=pcm&sample-rate=" + "44100"
        // }
        // );
        // console.log("signedURL= ", signedURL);
        // createMicrophoneStream();
        // sampleWebSocket(webSocket, setWebSocket, inputSampleRate, setInputSampleRate).start();
        // console.log("sampleWebSocket = ", sampleWebSocket(webSocket, setWebSocket, inputSampleRate, setInputSampleRate).start());
        
        return () => {
            if (mediaRecorder) {
                mediaRecorder.stop();
            }
        };
    }, [roomName])

    // const onTranscriptionDataReceived = (data) => {
    //     console.log("data = ", data);
    // }

    // const startRecord = async() => {
    //     await startRecording("en-US", onTranscriptionDataReceived);
    // }

    // const stopRecord = async() => {
    //     stopRecording();
    // }

    const cb = (obj) => {
        console.log("obj = ", obj);
    }

    const startRecord = async() => {
await transcribeController.init({callback: cb, roomName: roomName, localParticipant: {
                    name: localParticipant.identity
}
}).catch((error) => {
                    console.log(error);
                });
    }

    const stopRecord = async() => {
        await transcribeController.stop();
    }
    
    console.log("children = ", children, "messages = ", messages);
    return (
        <SymblContext.Provider
            value={{
                isConnected,// state only no use
                connectionId,
                startSymbl,
                stopSymbl,
                startSymblWebSocketApi,//nned to call in use effect
                stopSymblWebSocketApi,//needtostopstream
                muteSymbl,//needtocheckgainnodevalue
                unMuteSymbl,//needtocheckgainnodevalue
                isMute,//state to call abouve two fn
                startedTime,// show message.duration.startTime
                closedCaptionResponse,//need to ready this like closedCaptionResponse.punctuated.transcript || closedCaptionResponse.payload
                newMessages,//messages need
                messages,
                onError: onErrorCallback,
                onResultCallback: onResultCallback,
            }}
        >
            {/* <button onClick={() => (webSocket ? sampleWebSocket(webSocket, setWebSocket, inputSampleRate, setInputSampleRate).stop(webSocket) : sampleWebSocket(webSocket, setWebSocket, inputSampleRate, setInputSampleRate).start())}>{webSocket ? 'Stop' : 'Start'}</button> */}
            <button onClick={startRecord}>start</button>
            <button onClick={stopRecord}>stop</button>
            {children}
        </SymblContext.Provider>
    );
}

 // this.client = new TranscribeStreamingClient({
            //     region: this.region,
            //     credentials: {
            //         accessKeyId: 'YOUR ACCESS KEY',
            //         secretAccessKey: 'YOUR SECRET KEY'
            //     }
            // });
            
// closedCaptionResponse
//         stopSymblWebSocketApi, isConnected
//         muteSymbl, unMuteSymbl, isMute
//         {/* isStarting */}
//         newMessages, startedTime