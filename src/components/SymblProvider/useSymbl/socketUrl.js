//THIS SCRIPT IS BASED ON https://docs.aws.amazon.com/transcribe/latest/dg/websocket.html
// const aws = require('aws-sdk');
import crypto from 'crypto-js';
import queryString from 'query-string';
import moment from 'moment';

const awsRegion = 'us-west-2'
const accessKey = 'YOUR ACCESS KEY';
const secretAccessKey = 'YOUR SECRET KEY';

const createPresignedURL = async (method1, host1, path, service1, payload, options) => {
    // console.log(event);

    // let body = JSON.parse(event.body); I made a body object below for you to test with
    let body = {
        languageCode: "en-US", //or en-GB etc. I found en-US works better, even for British people due to the higher sample rate, which makes the audio clearer.
        sampleRate: 16000
    }

    // console.log(crypto.enc.Hex.stringify(signature_key));


    let method = "GET"
    let region = awsRegion;
    let endpoint = "wss://transcribestreaming." + region + ".amazonaws.com:8443"
    let host = "transcribestreaming." + region + ".amazonaws.com:8443"
    let amz_date = new moment().format('yyyyMMDDTHHmmss') + 'Z';
    let datestamp = new moment().format('yyyyMMDD');
    let service = 'transcribe';
    let linkExpirationSeconds = 60;
    // let signatureString = crypto.enc.Hex.stringify(signature_key);
    let languageCode = body.languageCode;
    let sampleRate = body.sampleRate
    let canonical_uri = "/stream-transcription-websocket"
    let canonical_headers = "host:" + host + "\n"
    let signed_headers = "host"
    let algorithm = "AWS4-HMAC-SHA256"
    let credential_scope = datestamp + "%2F" + region + "%2F" + service + "%2F" + "aws4_request"
    // Date and time of request - NOT url formatted
    let credential_scope2 = datestamp + "/" + region + "/" + service + "/" + "aws4_request"


    let canonical_querystring = "X-Amz-Algorithm=" + algorithm
    canonical_querystring += "&X-Amz-Credential=" + accessKey + "%2F" + credential_scope
    canonical_querystring += "&X-Amz-Date=" + amz_date
    canonical_querystring += "&X-Amz-Expires=" + linkExpirationSeconds
    canonical_querystring += "&X-Amz-SignedHeaders=" + signed_headers
    canonical_querystring += "&language-code=" + languageCode + "&media-encoding=pcm&sample-rate=" + sampleRate

    //Empty hash as playload is unknown
    let emptyHash = crypto.SHA256("");
    let payload_hash = crypto.enc.Hex.stringify(emptyHash);

    let canonical_request = method + '\n'
        + canonical_uri + '\n'
        + canonical_querystring + '\n'
        + canonical_headers + '\n'
        + signed_headers + '\n'
        + payload_hash

    let hashedCanonicalRequest = crypto.SHA256(canonical_request);

    let string_to_sign = algorithm + "\n"
        + amz_date + "\n"
        + credential_scope2 + "\n"
        + crypto.enc.Hex.stringify(hashedCanonicalRequest);

    //Create the signing key
    let signing_key = getSignatureKey(secretAccessKey, datestamp, region, service);

    //Sign the string_to_sign using the signing key
    let inBytes = crypto.HmacSHA256(string_to_sign, signing_key);

    let signature = crypto.enc.Hex.stringify(inBytes);

    canonical_querystring += "&X-Amz-Signature=" + signature;

    let request_url = endpoint + canonical_uri + "?" + canonical_querystring;

    //The final product
    console.log(request_url);

    // let response = {
    //     statusCode: 200,
    //     headers: {
    //       "Access-Control-Allow-Origin": "*"  
    //     },
    //     body: JSON.stringify(request_url)
    // };
    return request_url;
};

function getSignatureKey(key, dateStamp, regionName, serviceName) {
    var kDate = crypto.HmacSHA256(dateStamp, "AWS4" + key);
    var kRegion = crypto.HmacSHA256(regionName, kDate);
    var kService = crypto.HmacSHA256(serviceName, kRegion);
    var kSigning = crypto.HmacSHA256("aws4_request", kService);
    return kSigning;
};

export const url = createPresignedURL(
    'GET',
    "transcribestreaming." + "us-west-2" + ".amazonaws.com:8443",
    '/stream-transcription-websocket',
    'transcribe',
    'sha256', {
    'key': "YOUR ACCESS KEY",
    'secret': "YOUR SECRET KEY",
    'protocol': 'wss',
    'expires': 15,
    'region': "us-west-2",
    'query': "language-code=" + "en-US" + "&media-encoding=pcm&sample-rate=" + "44100"
}
);