import CryptoJS from 'crypto-js';
import queryString from 'query-string';

const createPresignedURL = (method, host, path, service, payload, options) => {
    options = options || {};
    options.key = options.key || process.env.AWS_ACCESS_KEY_ID;
    options.secret = options.secret || process.env.AWS_SECRET_ACCESS_KEY;
    options.protocol = options.protocol || 'https';
    options.headers = options.headers || {};
    options.timestamp = options.timestamp || Date.now();
    options.region = options.region || process.env.AWS_REGION || 'us-west-2';
    options.expires = options.expires || 86400; // 24 hours
    options.headers = options.headers || {};

    // host is required
    options.headers.Host = host;

    var query = options.query ? queryString.parse(options.query) : {};
    query['X-Amz-Algorithm'] = 'AWS4-HMAC-SHA256';
    query['X-Amz-Credential'] = options.key + '/' + createCredentialScope(options.timestamp, options.region, service);
    query['X-Amz-Date'] = toTime(options.timestamp);
    query['X-Amz-Expires'] = options.expires;
    query['X-Amz-SignedHeaders'] = createSignedHeaders(options.headers);
    if (options.sessionToken) {
        query['X-Amz-Security-Token'] = options.sessionToken;
    }

    var canonicalRequest = createCanonicalRequest(method, path, query, options.headers, payload);
    var stringToSign = createStringToSign(options.timestamp, options.region, service, canonicalRequest);
    var signature = createSignature(options.secret, options.timestamp, options.region, service, stringToSign);
    query['X-Amz-Signature'] = signature;
    return options.protocol + '://' + host + path + '?' + queryString.stringify(query);
};


const createCredentialScope = (time, region, service) => {
    return [toDate(time), region, service, 'aws4_request'].join('/');
}

const createSignedHeaders = (headers) => {
    return Object.keys(headers).sort().map(function (name) {
        return name.toLowerCase().trim();
    }).join(';');
}

const createCanonicalRequest = (method, pathname, query, headers, payload) => {
    return [
        method.toUpperCase(),
        pathname,
        createCanonicalQueryString(query),
        createCanonicalHeaders(headers),
        createSignedHeaders(headers),
        payload
    ].join('\n');
}

const createCanonicalQueryString = (params) => {
    return Object.keys(params).sort().map(function (key) {
        return encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
    }).join('&');
};

const createCanonicalHeaders = (headers) => {
    return Object.keys(headers).sort().map(function (name) {
        return name.toLowerCase().trim() + ':' + headers[name].toString().trim() + '\n';
    }).join('');
}

const createStringToSign = (time, region, service, request) => {
    return [
        'AWS4-HMAC-SHA256',
        toTime(time),
        createCredentialScope(time, region, service),
        createHash(request, 'hex')
    ].join('\n');
}

const createSignature = (secret, time, region, service, stringToSign) => {
    var h1 = createHmacString('AWS4' + secret, toDate(time)); // date-key
    var h2 = createHmacString(h1, region); // region-key
    var h3 = createHmacString(h2, service); // service-key
    var h4 = createHmacString(h3, 'aws4_request'); // signing-key
    return createHmacString(h4, stringToSign, 'hex');
}

function toTime(time) {
    return new Date(time).toISOString().replace(/[:\-]|\.\d{3}/g, '');
}

function toDate(time) {
    return toTime(time).substring(0, 8);
}

// function hmac(key, string, encoding) {
//     return crypto.createHmac('sha256', key)
//         .update(string, 'utf8')
//         .digest(encoding);
// }

function createHmacString(privateKey, ts) {
    const key = CryptoJS.enc.Utf8.parse(privateKey)
    const timestamp = CryptoJS.enc.Utf8.parse(ts)
    // const hmac = CryptoJS.enc.Hex.stringify(CryptoJS.HmacSHA256(timestamp, key))

    const hmac = CryptoJS.HmacSHA256(ts, privateKey).toString(CryptoJS.enc.Hex)
    return hmac;
}

function createHash(message) {
    // const hash = CryptoJS.enc.Hex.stringify(CryptoJS.SHA256(message));

    const hash = CryptoJS.SHA256(message).toString(CryptoJS.enc.Hex)
    return hash;
}


export const url = createPresignedURL(
    'GET',
    "transcribestreaming." + "us-west-2" + ".amazonaws.com:8443",
    '/stream-transcription-websocket',
    'transcribe',
    createHash('sha256'), {
    'key': "YOUR ACCESS KEY",
    'secret': "YOUR SECRET KEY",
    'protocol': 'wss',
    'expires': 15,
    'region': "us-west-2",
    'query': "language-code=" + "en-US" + "&media-encoding=pcm&sample-rate=" + "44100"
}
);