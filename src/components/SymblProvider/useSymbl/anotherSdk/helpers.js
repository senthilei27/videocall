/* eslint-disable no-await-in-loop */
/* eslint-disable @typescript-eslint/no-explicit-any */
// import { PassThrough } from 'stream';

export function mapRoute(text) {
    return `${text}-section`;
}

export async function* fromReadable(stream) {
    let exhausted = false;
    const onData = () =>
        new Promise((resolve) => {
            stream.once('data', (chunk) => {
                resolve(chunk);
            });
        });

    try {
        while (true) {
            const chunk = (await onData());
            if (chunk === null) {
                exhausted = true;
                break;
            }
            yield chunk;
        }
    } finally {
        if (!exhausted) {
            stream.destroy();
        }
    }
}

export function streamAsyncIterator(stream) {
    // Get a lock on the stream:
    //   const reader = stream.getReader();

    return {
        [Symbol.asyncIterator]() {
            return fromReadable(stream);
        },
    };
}
