import {
    TranscribeStreamingClient,
    StartStreamTranscriptionCommand,
    StartStreamTranscriptionCommandOutput,
} from '@aws-sdk/client-transcribe-streaming';
import MicrophoneStream from 'microphone-stream';
import { PassThrough } from 'stream';
import { EventEmitter } from 'events';

import { streamAsyncIterator } from './helpers';
import EncodePcmStream from './encodePcmStream';
import socketIOClient from 'socket.io-client';

class TranscribeController extends EventEmitter {

    constructor() {
        super();

        this.audioStream = null;
        this.rawMediaStream = null;
        this.audioPayloadStream = null;
        this.started = false;
        this.SAMPLE_RATE = 44100;
        this.socket = socketIOClient('http://192.168.0.221:8081'); // URL of your backend service


    }


    hasConfig() {
        return !!this.transcribeConfig;
    }

    setConfig(transcribeConfig) {
        this.transcribeConfig = transcribeConfig;
    }

    validateConfig() {
        if (
            !this.transcribeConfig?.accessKey ||
            !this.transcribeConfig.secretAccessKey
        ) {
            throw new Error(
                'missing required config: access key and secret access key are required',
            );
        }
    }

    async init(params) {
        this.started = true;
        // if (!this.transcribeConfig) {
        //     throw new Error('transcribe config is not set');
        // }

        // console.log('transcribe config', this.transcribeConfig);
        // this.validateConfig();

        // setting up microphone stream
        console.log('setting up microphone stream');
        this.audioStream = new MicrophoneStream();

        this.rawMediaStream = await window.navigator.mediaDevices.getUserMedia({
            video: false,
            audio: {
                sampleRate: this.SAMPLE_RATE,
            },
        });
        await this.audioStream.setStream(this.rawMediaStream);

        // create and pipe the streams
        console.log('setting up streams');
        this.audioPayloadStream = this.audioStream
            .pipe(new EncodePcmStream())
            .pipe(new PassThrough({ highWaterMark: 1 * 1024 }));

        // creating and setting up transcribe client
        const config = {
            region: 'us-west-2',
            credentials: {
                accessKeyId: 'YOUR ACCESS KEY',
                secretAccessKey: 'YOUR SECRET KEY'
            }
        };
        // console.log('setting up transcribe client with config', config);
        this.client = new TranscribeStreamingClient(config);

        const command = new StartStreamTranscriptionCommand({
            // The language code for the input audio. Valid values are en-GB, en-US, es-US, fr-CA, and fr-FR
            LanguageCode: "en-US",
            // The encoding used for the input audio. The only valid value is pcm.
            MediaEncoding: 'pcm',
            // The sample rate of the input audio in Hertz. We suggest that you use 8000 Hz for low-quality audio and 16000 Hz for
            // high-quality audio. The sample rate must match the sample rate in the audio file.
            MediaSampleRateHertz: this.SAMPLE_RATE,
            AudioStream: this.audioGenerator.bind(this)(),
            // ShowSpeakerLabel: true,
            // EnableChannelIdentification: true,
            // NumberOfChannels: 2,
            // VocabularyName: "TranscribeVocabulary1",
        });

        const response = await this.client.send(command);
        this.onStart(response, params);
        this.socket.on('connect', () => {
            console.log('Connected to server');
        });
    }

    async onStart(response, params) {
        console.log('recognition started', response, "params = ", params);

        if (response.TranscriptResultStream) {
            for await (const event of response.TranscriptResultStream) {
                // Get multiple possible results
                const results = event.TranscriptEvent?.Transcript?.Results;
                // Print all the possible transcripts
                // console.log('results', results);
                if (results && results.length > 0) {
                    const [result] = results;
                    const final = !result.IsPartial;
                    const alternatives = result.Alternatives;

                    if (alternatives && alternatives.length > 0) {
                        // const [alternative] = alternatives;
                        // const text = alternative.Transcript;

                        // params.callback({ text, final });
                        // this.socket.emit('audioData', Object.assign({}, { speaker: params.localParticipant.name, sentence: text,  }));

                        // this.socket.on('transcription', (transcription) => {
                        //     // Display the transcription for all speakers
                        //     console.log("transcription = ", transcription);
                        // });
                        if (final) {
                            const noOfResults = result.Alternatives[0].Items.length;
                            let sentence = '';
                            let speaker = '';
                            let arr = [];
                            for (let i = 0; i < noOfResults; i++) {
                                if (speaker == result.Alternatives[0].Items[i].Speaker) {
                                    // if (result.Alternatives[0].Items[i].Confidence) {
                                    sentence = sentence + " " + result.Alternatives[0].Items[i].Content;
                                    // } else {
                                    // sentence = sentence + " " + result.Alternatives[0].Items[i].Content;
                                    // }
                                } else if (!result.Alternatives[0].Items[i].Confidence) {
                                    sentence = sentence + "" + result.Alternatives[0].Items[i].Content;
                                } else {
                                    arr.push(Object.assign({}, { speaker: speaker, sentence: sentence }))
                                    speaker = result.Alternatives[0].Items[i].Speaker;
                                    // if (result.Alternatives[0].Items[i].Confidence) {
                                    sentence = result.Alternatives[0].Items[i].Content;
                                    // } else {
                                    // sentence = sentence + " " + result.Alternatives[0].Items[i].Content;
                                    // }
                                }
                                // console.log(result.Alternatives[0].Items[i].Content);
                                // console.log(result.Alternatives[0].Transcript);
                                params.callback({ sentence, speaker, arr });
                            }
                            console.log("arr = ", arr.push(Object.assign({}, { speaker: speaker, sentence: sentence })));
                            console.log("socket = ", this.socket);
                            this.socket.emit('audioData', Object.assign({}, { speaker: params.localParticipant.name, sentence: sentence, roomName: params.roomName }));

                            this.socket.on('transcription', (transcription) => {
                                // Display the transcription for all speakers
                                console.log("transcription = ", transcription);
                            });
                        }
                    }
                }

                // for (const result of event.TranscriptEvent.Transcript.Results || []) {
                //     if (result.IsPartial === false) {
                //         const noOfResults = result.Alternatives[0].Items.length;
                //         let sentence = '';
                //         let speaker = '';
                //         let arr = [];
                //         for (let i = 0; i < noOfResults; i++) {
                //             if (speaker == result.Alternatives[0].Items[i].Speaker) {
                //                 // if (result.Alternatives[0].Items[i].Confidence) {
                //                 sentence = sentence + " " + result.Alternatives[0].Items[i].Content;
                //                 // } else {
                //                 // sentence = sentence + " " + result.Alternatives[0].Items[i].Content;
                //                 // }
                //             } else if (!result.Alternatives[0].Items[i].Confidence) {
                //                 sentence = sentence + "" + result.Alternatives[0].Items[i].Content;
                //             } else {
                //                 arr.push(Object.assign({}, { speaker: speaker, sentence: sentence }))
                //                 speaker = result.Alternatives[0].Items[i].Speaker;
                //                 // if (result.Alternatives[0].Items[i].Confidence) {
                //                 sentence = result.Alternatives[0].Items[i].Content;
                //                 // } else {
                //                 // sentence = sentence + " " + result.Alternatives[0].Items[i].Content;
                //                 // }
                //             }
                //             // console.log(result.Alternatives[0].Items[i].Content);
                //             // console.log(result.Alternatives[0].Transcript);
                //             params.callback(result.Alternatives[0].Items[i].Content + " ");
                //         }
                //     }
                // }
            }
        }
    }

    async stop() {
        this.started = false;

        // request to stop recognition
        this.audioStream?.stop();
        this.audioStream = null;
        this.rawMediaStream = null;

        this.audioPayloadStream?.removeAllListeners();
        this.audioPayloadStream?.destroy();
        this.audioPayloadStream = null;

        this.client?.destroy();
        this.client = undefined;
    }

    async *audioGenerator() {
        if (!this.audioPayloadStream) {
            throw new Error('payload stream not created');
        }

        for await (const chunk of streamAsyncIterator(this.audioPayloadStream)) {
            if (this.started) {
                yield { AudioEvent: { AudioChunk: chunk } };
            } else {
                break;
            }
        }
    }
}

export default TranscribeController;
