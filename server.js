const express = require('express');
const app = express();
const fetch = require('node-fetch');
const path = require('path');
const AccessToken = require('twilio').jwt.AccessToken;
const VideoGrant = AccessToken.VideoGrant;
require('dotenv').config();
// const server = require('http').createServer();
// const io = require('socket.io')(server);
// const cors = require('cors');

// Enable CORS for all routes
// const corsOptions = {
//   origin: 'http://localhost:3000',
//   methods: ['GET', 'POST'],
// };
// const proxy = require('http-proxy-middleware');


const symblAppId = process.env.SYMBL_APP_ID;
const symblAppSecret = process.env.SYMBL_APP_SECRET;
const symblApiBasePath = process.env.SYMBL_API_BASE_PATH || 'https://api.symbl.ai';

const MAX_ALLOWED_SESSION_DURATION = 14400;
const twilioAccountSid = process.env.TWILIO_ACCOUNT_SID;
const twilioApiKeySID = process.env.TWILIO_API_KEY_SID;
const twilioApiKeySecret = process.env.TWILIO_API_KEY_SECRET;

app.use(express.static(path.join(__dirname, 'build')));

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, X-API-KEY");
    next();
});

// app.use(
//   '/socket.io',
//   proxy({
//     target: 'https://localhost:3001',
//     changeOrigin: true,
//     secure: false,
//     ws: true,
//     pathRewrite: {
//       '^/socket.io': ''
//     },
//     onProxyRes: function (proxyRes, req, res) {
//       proxyRes.headers['Access-Control-Allow-Origin'] = '*';
//     }
//   })
// );


app.get('/symbl-token', async (req, res) => {
    try {
        const response = await fetch(`${symblApiBasePath}/oauth2/token:generate`, {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json',
            },
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
            body: JSON.stringify({
                type: 'application',
                appId: symblAppId,
                appSecret: symblAppSecret
            })
        });
        res.json(await response.json()).end();
    } catch (e) {
        console.error('Error while issuing Symbl Token.', e);
        res.status(401)
            .json({
                message: e.toString()
            }).end()
    }
});

app.get('/twilio-token', (req, res) => {
    const { identity, roomName } = req.query;
    const token = new AccessToken(twilioAccountSid, twilioApiKeySID, twilioApiKeySecret, {
        ttl: MAX_ALLOWED_SESSION_DURATION,
    });
    token.identity = identity;
    const videoGrant = new VideoGrant({ room: roomName });
    token.addGrant(videoGrant);
    res.send(token.toJwt());
    console.log(`issued token for ${identity} in room ${roomName}`);
});

app.get('*', (_, res) => res.sendFile(path.join(__dirname, 'build/index.html')));

// // io.origins(corsOptions.origin); // Set the allowed origin for Socket.io
// server.use(cors); // Set the allowed origin for other routes

// // Socket.io connection handler
// io.on('connection', (socket) => {
//   // Receive audio data from the React application
//   socket.on('audioData', async (data) => {
//     console.log("data = ", data);
//     // Send the transcription back to the React application
//     socket.emit('transcription', data);
//   });
// });
// server.listen(3001, () => console.log('token server running on 3001'));
const server = app.listen(8081, () => console.log('token server running on 8081'));

io = require("socket.io")(server, {
    cors: {
        origin: "*",
    },
});

// Socket.io connection handler
io.on('connection', (socket) => {
    // Receive audio data from the React application
    socket.on('audioData', async (data) => {
        console.log("data = ", data);
        // Send the transcription back to the React application
        socket.emit('transcription', data);
    });
});
